import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'interview';
  active: number;
  cancel: number;
  circle1: string;
  circle2: string;
  circle3: string;
  circle4: string;

  activeCircle(id: number) {
    switch (id) {
      case 1:
        this.circle1 = 'active';
        break;
      case 2:
        this.circle2 = 'active';
        break;
      case 3:
        this.circle3 = 'active';
        break;
      case 4:
        this.circle4 = 'active';
        break;
      default:
        break;
    }
    this.cancelCircle(this.active)
    this.active = id;
  }

  cancelCircle(id: number) {
    switch (id) {
      case 1:
        this.circle1 = 'cancel';
        break;
      case 2:
        this.circle2 = 'cancel';
        break;
      case 3:
        this.circle3 = 'cancel';
        break;
      case 4:
        this.circle4 = 'cancel';
        break;
      default:
        break;
    }
    this.noneCircle(this.cancel)
    this.cancel = id;
  }

  noneCircle(id: number) {
    switch (id) {
      case 1:
        this.circle1 = 'none';
        break;

      case 2:
        this.circle2 = 'none';
        break;
      case 3:
        this.circle3 = 'none';
        break;
      case 4:
        this.circle4 = 'none';
        break;
      default:
        break;
    }
  }
}
